import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { bindActionCreators } from 'redux';
import homeActions from '../actions/home';
import settingsActions from '../actions/settings';
import Settings from '../components/Settings';

const mapStateToProps = (state) => {
  return state;
};

const mapDispatchToProps = (dispatch) => {
  const app = bindActionCreators(homeActions, dispatch);
  const settings = bindActionCreators(settingsActions, dispatch);
  return {
    openApp: () => {
      dispatch(push('/'));
    },
    updateDefaultCurrentPath: (dirname) => {
      settings.updatedefaultCurrentPath(dirname);
      app.setCurrentPath(dirname);
    },
  }
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Settings);
