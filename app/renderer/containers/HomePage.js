import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { bindActionCreators } from 'redux';
import homeActions from '../actions/home';
import App from '../components/App';


const mapStateToProps = (state) => {
  return state;
};

const mapDispatchToProps = (dispatch) => {
  const app = bindActionCreators(homeActions, dispatch);
  return {
    openSettings: () => {
      dispatch(push('/settings'));
    },
    setCurrentPath: (dirname) => {
      app.setCurrentPath(dirname);
    },
    appendToCurrentPath: (dirname) => {
      app.appendToCurrentPath(dirname);
    },
    popFromCurrentPath: (dirname) => {
      app.popFromCurrentPath(dirname);
    },
    activateStep: (step) => {
      app.activateStep(step);
    },
    selectFiles: (filenames) => {
      app.selectFiles(filenames);
    },
    setPreviewFiles: (filenames) => {
      app.setPreviewFiles(filenames);
    },
    setRulesValues: (values) => {
      app.setRulesValues(values);
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(App);
