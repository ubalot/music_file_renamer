import React from 'react';
import { Switch, Route } from 'react-router';
import HomePage from './containers/HomePage';
import SettingsPage from './containers/SettingsPage';

export default (
  <Switch>
    <Route exact path="/" component={HomePage} />
    <Route exact path="/settings" component={SettingsPage} />
  </Switch>
);
