import { createAction } from 'redux-actions';

export default {
  updatedefaultCurrentPath: createAction('UPDATE_DEFAULT_CURRENT_PATH'),
}