import { createAction } from 'redux-actions';

export default {
  setCurrentPath: createAction('SET_CURRENT_PATH'),
  appendToCurrentPath: createAction('APPEND_TO_CURRENT_PATH'),
  popFromCurrentPath: createAction('POP_FROM_CURRENT_PATH'),
  activateStep: createAction('ACTIVATE_STEP'),
  selectFiles: createAction('SELECT_FILES'),
  setPreviewFiles: createAction('SET_PREVIEW_FILES'),
  setRulesValues: createAction('SET_RULES_VALUES'),
};
