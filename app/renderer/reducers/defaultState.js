const fs = window.require('fs');
const os = window.require('os');

export const SUBSTITUTE = 'SUBSTITUTE';
export const PREFIX = 'PREFIX';
export const SUFFIX = 'SUFFIX';
export const ENUMERATE = 'ENUMERATE';
export const INSERT_AT = 'INSERT_AT';

export function sortSelectedRules(selectedRules) {
  return selectedRules.sort((a, b) => a.id > b.id);
}

// Default settings path.
const settingsPath = '../../settings.json';

// Set default settings.
let settings = {
  defaultCurrentPath: os.homedir(),
};

// If file settings.json exists then overwrite default settings.
if (fs.existsSync(settingsPath)) {
  const json = fs.readFileSync(settingsPath);
  if (json) {
    settings = JSON.parse(json);
  }
}

const home = {
  appTitle: 'Multiple File Renamer',
  currentPath: settings.defaultCurrentPath,
  activeStep: 0,
  selectedFiles: [],
  previewFiles: [],
  rules: sortSelectedRules([
    {
      name: SUBSTITUTE,
      data: {
        oldValue: '',
        newValue: ''
      }
    },
    {
      name: PREFIX,
      data: {
        value: ''
      }
    },
    {
      name: SUFFIX,
      data: {
        value: ''
      }
    },
    {
      name: ENUMERATE,
      data: {
        value: ''
      }
    },
    {
      name: INSERT_AT,
      data: {
        index: 0,
        value: '',
      }
    }
  ].map((d, i) => ({
    ...d,
    id: i,
    active: false
  })))
}

export default {
  home,
  settings
}