import { handleActions } from 'redux-actions';
import settings from '../actions/settings';
import defaultState from './defaultState';

const fs = window.require('fs');

export default handleActions(
  {
    [settings.updatedefaultCurrentPath]: (state, action) => {
      return fs.existsSync(action.payload)
        ? { ...state, defaultCurrentPath: action.payload }
        : state;
    },
  },
  defaultState,
);
