import { handleActions } from 'redux-actions';
import home from '../actions/home';
import defaultState from './defaultState';

const fs = window.require('fs');
const path = window.require('path');

export default handleActions(
  {
    [home.setCurrentPath]: (state, action) => {
      return { ...state, currentPath: action.payload };
    },
    [home.appendToCurrentPath]: (state, action) => {
      const newCurrentPath = path.join(state.currentPath, action.payload);
      return fs.existsSync(newCurrentPath)
        ? { ...state, currentPath: newCurrentPath }
        : state;
    },
    [home.popFromCurrentPath]: state => {
      const newCurrentPath = path.dirname(state.currentPath);
      return { ...state, currentPath: newCurrentPath };
    },
    [home.activateStep]: (state, action) => {
      return { ...state, activeStep: action.payload };
    },
    [home.selectFiles]: (state, action) => {
      return { ...state, selectedFiles: action.payload }
    },
    [home.setPreviewFiles]: (state, action) => {
      return { ...state, previewFiles: action.payload }
    },
    [home.setRulesValues]: (state, action) => {
      return { ...state, rules: action.payload };
    }
  },
  defaultState,
);
