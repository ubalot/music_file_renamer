import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepButton from '@material-ui/core/StepButton';

const styles = theme => ({
  root: {
    width: '90%'
  },
  button: {
    marginRight: theme.spacing.unit
  },
  backButton: {
    marginRight: theme.spacing.unit
  },
  completed: {
    display: 'inline-block'
  },
  instructions: {
    marginTop: theme.spacing.unit,
    marginBottom: theme.spacing.unit
  }
});

const getSteps = () => [
  'Select folder',
  'Select files',
  'Rename rules'
];

class HorizontalNonLinearAlternativeLabelStepper extends React.Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    activeStep: PropTypes.number.isRequired
  };

  state = {
    completed: new Set()
  };

  handleComplete = () => {
    // eslint-disable-next-line react/no-access-state-in-setstate
    const completed = new Set(this.state.completed);
    completed.add(this.state.activeStep);
    this.setState({
      completed
    });

    /**
     * Sigh... it would be much nicer to replace the following if conditional with
     * `if (!this.allStepsComplete())` however state is not set when we do this,
     * thus we have to resort to not being very DRY.
     */
    if (completed.size !== this.totalSteps() - this.skippedSteps()) {
      this.handleNext();
    }
  };

  handleReset = () => {
    this.setState({
      activeStep: 0,
      completed: new Set()
    });
  };

  isStepComplete = step => this.props.activeStep > step;

  completedSteps = () => this.state.completed.size;

  allStepsCompleted = () =>
    this.completedSteps() === this.totalSteps() - this.skippedSteps();

  isLastStep = () => this.state.activeStep === this.totalSteps() - 1;

  render() {
    const { classes, handleStep, activeStep } = this.props;
    const steps = getSteps();

    return (
      <div className={classes.root}>
        <Stepper alternativeLabel nonLinear activeStep={activeStep}>
          {steps.map((label, index) => {
            const props = {};
            const buttonProps = {};
            return (
              <Step key={label} {...props}>
                <StepButton
                  onClick={handleStep(index)}
                  completed={this.isStepComplete(index)}
                  {...buttonProps}
                >
                  {label}
                </StepButton>
              </Step>
            );
          })}
        </Stepper>
      </div>
    );
  }
}

export default withStyles(styles)(HorizontalNonLinearAlternativeLabelStepper);
