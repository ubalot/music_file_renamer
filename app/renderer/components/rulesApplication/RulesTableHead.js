import React from 'react';
import PropTypes from 'prop-types';
import TableHead from '@material-ui/core/TableHead';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';

const rows = [
  { name: 'name', numeric: false, disablePadding: true, label: 'Active' },
  { name: 'rule', numeric: false, disablePadding: true, label: 'Rule' },
  { name: 'value', numeric: false, disablePadding: true, label: 'Value' }
];

class EnhancedTableHead extends React.Component {
  static propTypes = {
    onRequestSort: PropTypes.func.isRequired,
    order: PropTypes.string.isRequired,
    orderBy: PropTypes.string.isRequired,
    rowCount: PropTypes.number.isRequired
  };

  createSortHandler = property => event => {
    this.props.onRequestSort(event, property);
  };

  render() {
    return (
      <TableHead>
        <TableRow>
          {rows.map((row, index) =>
              <TableCell
                key={index}
                numeric={row.numeric}
                padding={'default'}
              >
                {row.label}
              </TableCell>
          )}
        </TableRow>
      </TableHead>
    );
  }
}

export default EnhancedTableHead;
