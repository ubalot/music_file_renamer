import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import CustomizedEnhancedTable from './CustomizedTable';

class PreviewGrid extends Component {
  static propTypes = {
    previewFiles: PropTypes.array.isRequired,
    selectedFiles: PropTypes.array.isRequired,
  }

  state = {
    order: 'asc',
    orderBy: 'name',
    page: 0,
    rowsPerPage: 5
  }

  handleRequestSort = (event, property) => {
    const orderBy = property;
    let order = 'desc';

    if (this.state.orderBy === property && this.state.order === 'desc') {
      order = 'asc';
    }

    this.setState({ order, orderBy });
  };

  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };

  render() {
    const { previewFiles, selectedFiles } = this.props;
    const { order, orderBy, page, rowsPerPage } = this.state;

    return (
      <Grid container spacing={24}>
        <Grid item xs={6}>
          <CustomizedEnhancedTable
            title="Original"
            order={order}
            orderBy={orderBy}
            page={page}
            rowsPerPage={rowsPerPage}
            data={selectedFiles}
            handleRequestSort={this.handleRequestSort}
            handleChangePage={this.handleChangePage}
            handleChangeRowsPerPage={this.handleChangeRowsPerPage}
          />
        </Grid>
        <Grid item xs={6}>
          <CustomizedEnhancedTable
            title="Preview"
            order={order}
            orderBy={orderBy}
            page={page}
            rowsPerPage={rowsPerPage}
            data={previewFiles}
            handleRequestSort={this.handleRequestSort}
            handleChangePage={this.handleChangePage}
            handleChangeRowsPerPage={this.handleChangeRowsPerPage}
          />
        </Grid>
      </Grid>
    );
  }
}

export default PreviewGrid