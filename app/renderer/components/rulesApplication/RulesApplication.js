import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ExpansionPanel from './ExpansionPanel';
import RulesTable from './RulesTable';
import { SUBSTITUTE, PREFIX, SUFFIX, ENUMERATE, INSERT_AT } from '../../reducers/defaultState';
import PreviewGrid from './PreviewGrid';

const fs = window.require('fs');
const path = window.require('path');

class RulesApplication extends Component {
  static propTypes = {
    selectedFiles: PropTypes.array,
    previewFiles: PropTypes.array,
    rules: PropTypes.array,
    setPreviewFiles: PropTypes.func,
    currentPath: PropTypes.string
  }

  applyRulesToPreviews = () => {
    const { rules, setPreviewFiles, selectedFiles } = this.props;

    let newPreviewFiles = selectedFiles;

    rules.forEach(rule => {
      if (!rule.active) {
        return;
      }

      if (rule.name === SUBSTITUTE) {
        const { oldValue, newValue } = rule.data;
        if (!oldValue) return;
        newPreviewFiles =
          newPreviewFiles.map(filename => filename.replace(oldValue, newValue));
      } else if (rule.name === PREFIX) {
        const { value } = rule.data;
        newPreviewFiles =
          newPreviewFiles.map(filename => value.concat(filename));
      } else if (rule.name === SUFFIX) {
        const { value } = rule.data;
        newPreviewFiles =
          newPreviewFiles.map(filename => filename.concat(value));
      } else if (rule.name === ENUMERATE) {
        const { value } = rule.data;
        newPreviewFiles = newPreviewFiles.map((filename, index) =>
          rule.value
          ? index.toString().padStart(parseInt(value), '0').concat(' ', filename)
          : index.concat(' ', filename));
      } else if (rule.name === INSERT_AT) {
        const { index, value } = rule.data;
        if (index <= 0) {
          newPreviewFiles = newPreviewFiles.map(filename => value.concat(filename));
        } else if (index > 0 ) {
          const actualIndex = index - 1;
          newPreviewFiles = newPreviewFiles.map(filename =>
            filename.substring(0, actualIndex)
              .concat(value)
              .concat(filename.substring(actualIndex, filename.length))
          );
        } else {
          newPreviewFiles = newPreviewFiles.map(filename => filename.concat(value))
        }
      }
    });

    setPreviewFiles(newPreviewFiles);
  }

  applyRules = () => {
    const { selectedFiles, previewFiles, currentPath } = this.props;

    const zippedFiles = selectedFiles.map((src, i) => {
      return {
        src,
        dst: previewFiles[i]
      };
    });

    zippedFiles.forEach(data => {
      const { src, dst } = data;
      const srcPath = path.join(currentPath, src);
      const dstPath = path.join(currentPath, dst);
      fs.renameSync(srcPath, dstPath);
    });
  }

  render() {
    const { selectedFiles, rules, previewFiles, setRulesValues, setPreviewFiles } = this.props;

    return (
      <div>
        <ExpansionPanel title="Preview">
          <PreviewGrid
            previewFiles={previewFiles}
            selectedFiles={selectedFiles}
          />
        </ExpansionPanel>
        <RulesTable
          title="Rename Rules"
          rules={rules}
          applyRulesToPreviews={this.applyRulesToPreviews}
          applyRules={this.applyRules}
          setRulesValues={setRulesValues}
          selectedFiles={selectedFiles}
          setPreviewFiles={setPreviewFiles}
          numSelected={selectedFiles.length}
        />
      </div>
    );
  }
}

export default RulesApplication;
