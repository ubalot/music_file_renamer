import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Checkbox from '@material-ui/core/Checkbox';
import EnhancedTableToolbar from './RulesTableToolbar';
import EnhancedTableHead from './RulesTableHead';
import FormControl from '@material-ui/core/FormControl';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { sortSelectedRules, SUBSTITUTE, INSERT_AT } from '../../reducers/defaultState';

class SingleInput extends Component {
  static propTypes = {
    handleRuleValueChange: PropTypes.func.isRequired,
    rule: PropTypes.object.isRequired,
    handleEnterKeyDown: PropTypes.func
  };

  render() {
    const { handleRuleValueChange, rule } = this.props;

    return (
      <TextField
        label="Text"
        value={rule.data.value}
        onKeyDown={this.props.handleEnterKeyDown}
        onChange={handleRuleValueChange(rule.name)}
      />
    );
  }
}

class DoubleInput extends Component {
  static propTypes = {
    handleRuleValueChange: PropTypes.func.isRequired,
    rule: PropTypes.object.isRequired,
    handleEnterKeyDown: PropTypes.func
  };

  render() {
    const { handleRuleValueChange, rule } = this.props;

    const { oldValue, newValue } = rule.data;

    return (
      <div>
        <div>
          <TextField
            label="old value"
            value={oldValue}
            onKeyDown={this.props.handleEnterKeyDown}
            onChange={handleRuleValueChange(rule.name, 0)}
          />
        </div>
        <div>
          <TextField
            label="new value"
            value={newValue}
            onKeyDown={this.props.handleEnterKeyDown}
            onChange={handleRuleValueChange(rule.name, 1)}
          />
        </div>
      </div>
    );
  }
}

class InsertAtInput extends Component {
  static propTypes = {
    handleRuleValueChange: PropTypes.func.isRequired,
    rule: PropTypes.object.isRequired,
    handleEnterKeyDown: PropTypes.func
  };

  render() {
    const { handleRuleValueChange, rule } = this.props;

    return (
      <div>
        <div>
          <TextField
            label="Index"
            value={rule.data.index}
            type="number"
            margin="normal"
            onChange={handleRuleValueChange(rule.name, 0)}
          />
        </div>
        <div>
          <TextField
            label="Text"
            value={rule.data.value}
            onKeyDown={this.props.handleEnterKeyDown}
            onChange={handleRuleValueChange(rule.name, 1)}
            margin="normal"
          />
        </div>
      </div>
    );
  }
}


const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3
  },
  tableWrapper: {
    overflowX: 'auto'
  },
  button: {
    margin: theme.spacing.unit,
  },
});

class RulesTable extends React.Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    rules: PropTypes.array.isRequired,
    setPreviewFiles: PropTypes.func.isRequired,
    selectedFiles: PropTypes.array.isRequired,
    setRulesValues: PropTypes.func.isRequired,
    applyRulesToPreviews: PropTypes.func.isRequired
  };

  state = {
    order: 'asc',
    orderBy: 'name',
    selected: this.props.rules.filter(d => d.active).map((d, i) => i),
    page: 0,
    rowsPerPage: this.props.rules.length
  };

  updateSelected = () => {
    this.setState({
      selected: this.props.rules.filter(d => d.active).map((d, i) => i)
    });
  }

  handleRequestSort = (event, property) => {
    const orderBy = property;
    let order = 'desc';

    if (this.state.orderBy === property && this.state.order === 'desc') {
      order = 'asc';
    }

    this.setState({ order, orderBy });
  };

  handleClick = id => () => {
    const { rules, setRulesValues } = this.props;

    const newSelectedRules = rules.map((d, i) => ({
      ...d,
      active: i === id ? !d.active : d.active
    }));

    setRulesValues(newSelectedRules);
  };

  handleRuleValueChange = (name, pos = 0) =>
    event => {
      const { rules, setRulesValues } = this.props;
      const { value } = event.target;

      const immutateRules = rules.filter(rule => rule.name !== name);
      const targetRule = rules.find(rule => rule.name === name);

      let finalState = immutateRules;

      if (name === SUBSTITUTE) {
        const data = {};
        if (pos === 0) {
          data.oldValue = value;
          data.newValue = targetRule.data.newValue;
        } else if (pos === 1) {
          data.oldValue = targetRule.data.oldValue;
          data.newValue = value;
        }

        finalState = sortSelectedRules(immutateRules.concat({
          id: targetRule.id,
          name,
          data,
          active: true
        }));
      } else if (name === INSERT_AT) {
        const data = {};
        if (pos === 0) {
          data.index = parseInt(value);
          data.value = targetRule.data.value;
        } else if (pos === 1) {
          data.index = targetRule.data.index;
          data.value = value;
        }

        finalState = immutateRules.concat({
          id: targetRule.id,
          name,
          data,
          active: true
        })
      } else {
        finalState = immutateRules.concat({
          id: targetRule.id,
          name,
          data: {
            value
          },
          active: true
        });
      }

      setRulesValues(finalState);
      this.updateSelected();
    };

  // If enter is pressed, we show the preview.
  handleEnterKeyDown = event => {
    if (event.keyCode === 13) {
      this.props.applyRulesToPreviews();
    }
  }

  isSelected = id => {
    const { rules } = this.props;
    const rule = rules.find((d, i) => i === id);
    return rule ? rule.active : false;
  }

  render() {
    const { classes, rules, applyRulesToPreviews, applyRules } = this.props;
    const { order, orderBy, selected } = this.state;

    return (
      <Paper className={classes.root}>
        <EnhancedTableToolbar />
        <div className={classes.tableWrapper}>
          <Table className={classes.table} aria-labelledby="tableTitle">
            <EnhancedTableHead
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={this.handleSelectAllClick}
              onRequestSort={this.handleRequestSort}
              rowCount={rules.length}
            />
            <TableBody>
              {rules
                .map(rule => {
                  const isSelected = this.isSelected(rule.id);
                  return (
                    <TableRow
                      hover
                      role="checkbox"
                      aria-checked={isSelected}
                      tabIndex={-1}
                      key={rule.id}
                      selected={isSelected}
                    >
                      <TableCell padding="checkbox"
                        onClick={this.handleClick(rule.id)}
                      >
                        <Checkbox checked={isSelected} />
                      </TableCell>
                      <TableCell component="th" scope="row" padding="none">
                        {rule.name}
                      </TableCell>
                      <TableCell>
                      <FormControl className={classes.formControl} aria-describedby="name-helper-text">
                        {rule.name === SUBSTITUTE &&
                          <DoubleInput
                            handleRuleValueChange={this.handleRuleValueChange}
                            handleEnterKeyDown={this.handleEnterKeyDown}
                            rule={rule}
                          />}
                        {rule.name === INSERT_AT &&
                          <InsertAtInput
                            handleRuleValueChange={this.handleRuleValueChange}
                            handleEnterKeyDown={this.handleEnterKeyDown}
                            rule={rule}
                          />}
                        {rule.name !== SUBSTITUTE && rule.name !== INSERT_AT &&
                        <SingleInput
                          handleRuleValueChange={this.handleRuleValueChange}
                          handleEnterKeyDown={this.handleEnterKeyDown}
                          rule={rule}
                        />}
                      </FormControl>
                      </TableCell>
                    </TableRow>
                  );
                })}
            </TableBody>
          </Table>
        </div>
        <Button
          className={classes.button}
          color="primary"
          variant="contained"
          onClick={applyRulesToPreviews}
        >
          Preview
        </Button>
        <Button
          className={classes.button}
          color="secondary"
          variant="contained"
          onClick={applyRules}
        >
          Apply
        </Button>
      </Paper>
    );
  }
}

export default withStyles(styles)(RulesTable);
