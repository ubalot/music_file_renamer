import React, { Component } from 'react';
import PropTypes from 'prop-types';
import SettingsIcon from '@material-ui/icons/Settings';
import DenseAppBar from './AppBar';
import HorizontalLinearStepper from './Stepper';
import DirectorySelection from './directorySelection/DirectorySelection';
import FileSelection from './filesSelection/FilesSelection';
import RulesApplication from './rulesApplication/RulesApplication';

class App extends Component {

  static propTypes = {
    home: PropTypes.object.isRequired,
    appendToCurrentPath: PropTypes.func.isRequired,
    popFromCurrentPath: PropTypes.func.isRequired,
    selectFiles: PropTypes.func.isRequired,
    setPreviewFiles: PropTypes.func.isRequired,
    setRulesValues: PropTypes.func.isRequired,
  };

  renderStep = () => {
    const {
      appendToCurrentPath,
      popFromCurrentPath,
      selectFiles,
      setPreviewFiles,
      setRulesValues,
    } = this.props;

    const {
      activeStep,
      currentPath,
      selectedFiles,
      rules,
      previewFiles
    } = this.props.home;

    const step0 = <DirectorySelection
                    currentPath={currentPath}
                    appendToCurrentPath={appendToCurrentPath}
                    popFromCurrentPath={popFromCurrentPath}
                  />;

    const step1 = <FileSelection
                    currentPath={currentPath}
                    selectFiles={selectFiles}
                    selectedFiles={selectedFiles}
                    setPreviewFiles={setPreviewFiles} />;

    const step2 = <RulesApplication
                    currentPath={currentPath}
                    selectedFiles={selectedFiles}
                    previewFiles={previewFiles}
                    rules={rules}
                    setPreviewFiles={setPreviewFiles}
                    setRulesValues={setRulesValues} />;

    switch (activeStep) {
      case 0:
        return step0;
      case 1:
        return step1;
      case 2:
        return step2;
      default:
        return step0;
    }
  };

  handleStep = step => () => {
    this.props.activateStep(step);
  };

  render() {
    const { openSettings } = this.props;
    const { activeStep, appTitle } = this.props.home;

    return (
      <div id='app'>
        <DenseAppBar handleClick={openSettings} title={appTitle}>
          <SettingsIcon />
        </DenseAppBar>
        <div id="horizontal-stepper">
          <HorizontalLinearStepper
            activeStep={activeStep}
            handleStep={this.handleStep}
          />
        </div>
        <div id="folder-select">
          {this.renderStep()}
        </div>
      </div>
    );
  }
}

export default App;
