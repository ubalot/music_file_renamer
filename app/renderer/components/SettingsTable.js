import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import {
  Edit as EditIcon,
  Done as DoneIcon,
  Cancel as CancelIcon
} from '@material-ui/icons';

class SettingRow extends Component {
  static propTypes = {
    defaultValue: PropTypes.string.isRequired,
    handleAcceptChange: PropTypes.func.isRequired
  }

  state = {
    defaultValue: this.props.value,
    value: '',
    editMode: false
  }

  handleEditClick = () => {
    this.setState({ editMode: true });
  }

  handleChange = event => {
    const { value } = event.target;

    this.setState({ value });
  }

  handleAcceptChange = () => {
    const { handleAcceptChange } = this.props;
    const { value } = this.state;

    handleAcceptChange(value);

    this.setState({ editMode: false });
  }

  handleCancelChange = () => {
    this.setState({ value: '', editMode: false });
  }

  render() {
    const { name, defaultValue } = this.props;
    const { editMode, value } = this.state;

    return (
      <TableRow>
        <TableCell component="th" scope="row">
          {name}
        </TableCell>
        {editMode
        ?
          <TableCell>
            <TextField
              label={defaultValue}
              value={value}
              onChange={this.handleChange}
              margin="normal"
            />
          </TableCell>
          :
            <TableCell>
              {defaultValue}
            </TableCell>
        }
        {editMode
        ?
          <TableCell>
            <Button onClick={this.handleAcceptChange}>
              <DoneIcon />
            </Button>
            <Button onClick={this.handleCancelChange}>
              <CancelIcon />
            </Button>
          </TableCell>
        :
          <TableCell>
            <Button onClick={this.handleEditClick}>
              <EditIcon />
            </Button>
          </TableCell>
        }
      </TableRow>
    );
  }
}


const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
  },
});

class SimpleTable extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    data: PropTypes.array,
  };

  render() {
    const { classes, data } = this.props;

    const rows = data.map((d, i) => ({
      id: i,
      name: d.name,
      value: d.value,
      func: d.func
    }));

    return (
      <Paper className={classes.root}>
        <Table className={classes.table}>
          <TableHead>
            <TableRow>
              <TableCell>Setting</TableCell>
              <TableCell>Value</TableCell>
              <TableCell>Edit</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map(row =>
              <SettingRow
                key={row.id}
                name={row.name}
                defaultValue={row.value}
                handleAcceptChange={row.func}
              />
            )}
          </TableBody>
        </Table>
      </Paper>
    );
  }
}

export default withStyles(styles)(SimpleTable);