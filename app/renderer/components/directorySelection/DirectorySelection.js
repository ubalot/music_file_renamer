import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import SubdirectoryArrowLeftIcon from '@material-ui/icons/SubdirectoryArrowLeft';
import IntegrationAutosuggest from './SearchBox';

const styles = theme => ({
  root: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2
  },
  button: {
    margin: theme.spacing.unit,
  },
  input: {
    display: 'none'
  },
  iconSmall: {
    fontSize: 20,
  },
});

class DirectorySelection extends Component {

  static propTypes = {
    classes: PropTypes.object.isRequired,
    currentPath: PropTypes.string.isRequired,
    appendToCurrentPath: PropTypes.func.isRequired,
  };

  handleDeleteClick = () => {
    const { popFromCurrentPath } = this.props;
    popFromCurrentPath();
  }

  render() {
    const { classes, currentPath, appendToCurrentPath } = this.props;

    return (
      <Paper className={classes.root} elevation={1}>
        <Typography variant="display1" component="div">
          {currentPath}
          <Button
            size="small"
            variant="contained"
            color="primary"
            className={classes.button}
            onClick={this.handleDeleteClick}>
            <SubdirectoryArrowLeftIcon
              fontSize="small"
              className={classes.rightIcon}
            />
          </Button>
        </Typography>
        <Typography>
          <IntegrationAutosuggest
            currentPath={currentPath}
            appendToCurrentPath={appendToCurrentPath}
          />
        </Typography>
      </Paper>
    );
  }
}

export default withStyles(styles)(DirectorySelection);
