import React, { Component } from "react";
import PropTypes from 'prop-types';
import HomeIcon from '@material-ui/icons/Home';
import DenseAppBar from './AppBar';
import SimpleTable from './SettingsTable';

class Settings extends Component {
  static propTypes = {
    openApp: PropTypes.func.isRequired,
  }

  state = {
    title: 'Settings',
  }

  render() {
    const { openApp, settings, updateDefaultCurrentPath } = this.props;
    const { title } = this.state;

    const data = [];
    for (const key in settings) {
      if (settings.hasOwnProperty(key)) {
        let func;
        switch (key) {
          case 'defaultCurrentPath':
            func = updateDefaultCurrentPath
        }

        data.push({
          name: key,
          value: settings[key],
          func
        })
      }
    }
    return (
      <div id='settings'>
        <DenseAppBar title={title} handleClick={openApp}>
          <HomeIcon />
        </DenseAppBar>
        <SimpleTable data={data} />
      </div>
    );
  }
}

export default Settings;
