# Multiple file renamer

## Start project

### Install dependencies
```
yarn install
```

### Start dev mode
```
yarn develop
```

### Build project
Single target OS
```
yarn build:linux
yarn build:mac
yarn build:windows
```
or all-in-one command
```
yarn build
```
