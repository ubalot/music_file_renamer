import { expect } from 'chai';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import actions from '../../app/renderer/actions/settings';
import defaultState from '../../app/renderer/reducers/defaultState';

const mockStore = configureMockStore([thunk]);

describe('actions', () => {
  describe('update default current path', () => {
    it('should update defaultCurrentPath', () => {
      const store = mockStore(defaultState);
      const expectedActions = [
        {
          type: 'UPDATE_DEFAULT_CURRENT_PATH',
          payload: '/home/test'
        }
      ];

      store.dispatch(
        actions.updatedefaultCurrentPath('/home/test')
      )

      expect(store.getActions()).deep.equal(expectedActions);
    });
  });
});
