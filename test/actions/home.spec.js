import { expect } from 'chai';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import actions from '../../app/renderer/actions/home';
import defaultState from '../../app/renderer/reducers/defaultState';

const mockStore = configureMockStore([thunk]);

describe('actions', () => {
  describe('home', () => {
    it('set current path', () => {
      const store = mockStore(defaultState);

      const expectedActions = [
        {
          type: 'SET_CURRENT_PATH',
          payload: '/home/test'
        }
      ];

      store.dispatch(
        actions.setCurrentPath('/home/test')
      );

      expect(store.getActions()).deep.equal(expectedActions);
    });

    it('appendToCurrentPath', () => {
      const store = mockStore(defaultState);

      const expectedActions = [
        {
          type: 'APPEND_TO_CURRENT_PATH',
          payload: 'test'
        }
      ];

      store.dispatch(
        actions.appendToCurrentPath('test')
      );

      expect(store.getActions()).deep.equal(expectedActions);
    });
  });

  it('popFromCurrentPath', () => {
    const store = mockStore(defaultState);
    const expectedActions = [
      {
        type: 'POP_FROM_CURRENT_PATH'
      }
    ];

    store.dispatch(
      actions.popFromCurrentPath()
    );

    expect(store.getActions()).deep.equal(expectedActions);
  });

  it('activateStep', () => {
    const store = mockStore(defaultState);
    const expectedActions = [
      {
        type: 'ACTIVATE_STEP',
        payload: 1
      }
    ];

    store.dispatch(
      actions.activateStep(1)
    );

    expect(store.getActions()).deep.equal(expectedActions);
  });

  it('selectFiles', () => {
    const store = mockStore(defaultState);
    const expectedActions = [
      {
        type: 'SELECT_FILES',
        payload: [
          '/home/test/file.txt',
          '/home/test/file.log',
          '/home/test/file.backup'
        ]
      }
    ];

    store.dispatch(
      actions.selectFiles([
        '/home/test/file.txt',
        '/home/test/file.log',
        '/home/test/file.backup'
      ])
    );

    expect(store.getActions()).deep.equal(expectedActions);
  });

  it('setPreviewFiles', () => {
    const store = mockStore(defaultState);
    const expectedActions = [
      {
        type: 'SET_PREVIEW_FILES',
        payload: [
          '/home/test/file.txt',
          '/home/test/file.log',
          '/home/test/file.backup'
        ]
      }
    ];

    store.dispatch(
      actions.setPreviewFiles([
        '/home/test/file.txt',
        '/home/test/file.log',
        '/home/test/file.backup'
      ])
    );

    expect(store.getActions()).deep.equal(expectedActions);
  });

  it('setRulesValues', () => {
    const store = mockStore(defaultState);

    const rules = Object.assign(defaultState.home.rules, {
      id: 0,
      name: 'SUBSTITUTE',
      data: {
        oldValue: 'a',
        newValue: 'b'
      },
      active: true
    });

    const expectedActions = [
      {
        type: 'SET_RULES_VALUES',
        payload: rules
      }
    ];

    store.dispatch(
      actions.setRulesValues(rules)
    );

    expect(store.getActions()).deep.equal(expectedActions);
  });
});
