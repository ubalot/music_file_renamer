import { expect } from 'chai';
import reducer from '../../app/renderer/reducers/home';
import defaultState from '../../app/renderer/reducers/defaultState';

describe('reducers', () => {
  describe('home', () => {
    it('should handle SET_CURRENT_PATH', () => {
      const action = {
        type: 'SET_CURRENT_PATH',
        payload: '/home/test'
      };

      const test = Object.assign(defaultState.home, {
        currentPath: action.payload
      });

      expect(reducer(defaultState.home, action)).to.deep.equal(test);
    });

    it('should handle APPEND_TO_CURRENT_PATH', () => {
      const action = {
        type: 'APPENT_TO_CURRENT_PATH',
        payload: 'rest'
      };

      const test = Object.assign(defaultState.home, {
        currentPath: `${defaultState.home.currentPath}/rest`
      });

      expect(reducer(defaultState.home, action)).to.deep.equal(test);
    });

    it('should handle POP_FROM_CURRENT_PATH', () => {
      const action = {
        type: 'POP_FROM_CURRENT_PATH'
      };

      const test = Object.assign(defaultState.home, {
        currentPath: '/'
      });

      expect(reducer(defaultState.home, action)).to.deep.equal(test);
    });

    it('should handle ACTIVATE_STEP', () => {
      const action = {
        type: 'ACTIVATE_STEP',
        payload: 2
      };

      const test = Object.assign(defaultState.home, { activeStep: 2 });

      expect(reducer(defaultState.home, action)).to.deep.equal(test);
    });

    it('should handle SELECT_FILES', () => {
      const filenames = [
        'file.txt',
        'file.log',
        'file.backup'
      ];

      const action = {
        type: 'SELECT_FILES',
        payload: filenames
      };

      const test = Object.assign(defaultState.home, {
        selectedFiles: filenames
      });

      expect(reducer(defaultState.home, action)).to.deep.equal(test);
    });

    it('should handle SET_PREVIEW_FILES', () => {
      const filenames = [
        'file.txt',
        'file.log',
        'file.backup'
      ];

      const action = {
        type: 'SET_PREVIEW_FILES',
        payload: filenames
      };

      const test = Object.assign(defaultState.home, {
        previewFiles: filenames
      });

      expect(reducer(defaultState.home, action)).to.deep.equal(test);
    });

    it('should handle SET_RULES_VALUES', () => {
      const rules = Object.assign(defaultState.home.rules, {
        id: 0,
        name: 'SUBSTITUTE',
        data: {
          oldValue: 'a',
          newValue: 'b'
        },
        active: true
      });

      const action = {
        type: 'SET_RULES_VALUES',
        payload: rules
      };

      const test = Object.assign(defaultState.home, rules);

      expect(reducer(defaultState.home, action)).to.deep.equal(test);
    });
  });
});
