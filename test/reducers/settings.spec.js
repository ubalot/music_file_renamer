import { expect } from 'chai';
import reducer from '../../app/renderer/reducers/settings';
import defaultState from '../../app/renderer/reducers/defaultState';

describe('reducers', () => {
  describe('settings', () => {
    it('should handle SET_CURRENT_PATH', () => {
      const action = {
        type: 'UPDATE_DEFAULT_CURRENT_PATH',
        payload: '/home/test'
      };

      const test = Object.assign(defaultState, {
        'currentPath': action.payload
      });

      expect(reducer(defaultState, action)).to.deep.equal(test);
    });
  });
});
